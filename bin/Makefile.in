SHELL = @PORTAGE_BASH@

prefix = @prefix@
exec_prefix = @exec_prefix@
sysconfdir = @sysconfdir@
libdir = @libdir@

srcdir = @srcdir@
top_builddir = @top_builddir@

portageuser = @portageuser@
portagegroup = @portagegroup@

PORTAGE_BIN = @PORTAGE_BASE@/bin
LN_S = @LN_S@
INSTALL = @INSTALL@
INSTALL_subst = $(top_builddir)/subst-install

usr_binprogs = \
	ebuild \
	egencache \
	emerge \
	emerge-webrsync \
	portageq \
	quickpkg \
	repoman

usr_sbinprogs = \
	archive-conf \
	dispatch-conf \
	emaint \
	env-update \
	etc-update \
	fixpackages \
	readpecoff \
	regenworld

# This is just a lazy hack for not having to list all files individually
# FIXME: make it a list (so it becomes explicit) and use an
# AM_CONDITIONAL to not include the sed wrapper in it when a Prefix is
# active.
list_sourcedir_dirs = \
	( cd "$(srcdir)" && find . -name '.svn' -prune -o -type d -print )

all:

install:
	$(INSTALL) -d -m 755 -o "$(portageuser)" -g "$(portagegroup)" $(DESTDIR)$(PORTAGE_BIN)
	$(list_sourcedir_dirs) | while read f ; do \
		files=( ) ; \
		for t in "$(srcdir)/$${f}"/* ; do \
			[[ -d $${t} ]] && continue ; \
			[[ $${t} == */Makefile* ]] && continue ; \
			files=( "$${files[@]}" "$${t}" ) ; \
		done ; \
		$(INSTALL) -d -m 755 \
			-o "$(portageuser)" -g "$(portagegroup)" \
			"$(DESTDIR)$(PORTAGE_BIN)/$${f}" && \
		$(INSTALL_subst) -m 755 \
			-o "$(portageuser)" -g "$(portagegroup)" \
			-t "$(DESTDIR)$(PORTAGE_BIN)/$${f}" \
			"$${files[@]}" ; \
	done
	$(INSTALL) -d -m 755 -o "$(portageuser)" -g "$(portagegroup)" $(DESTDIR)$(prefix)/bin
	cd $(DESTDIR)$(prefix)/bin \
	; for p in $(usr_binprogs) \
	; do test -f $(DESTDIR)$(PORTAGE_BIN)/$${p} \
		 || { echo "$(DESTDIR)$(PORTAGE_BIN)/$${p} does not exist" ; exit 1 ; } \
	   ; rm -f $(DESTDIR)$(prefix)/bin/$${p} \
	   ; $(LN_S) ../lib/portage/bin/$${p} $${p} || exit 1 \
	; done
	$(INSTALL) -d -m 755 -o "$(portageuser)" -g "$(portagegroup)" $(DESTDIR)$(prefix)/sbin
	cd $(DESTDIR)$(prefix)/sbin \
	; for p in $(usr_sbinprogs) \
	; do test -f $(DESTDIR)$(PORTAGE_BIN)/$${p} \
		 || { echo "$(DESTDIR)$(PORTAGE_BIN)/$${p} does not exist" ; exit 1 ; } \
	   ; rm -f $(DESTDIR)$(prefix)/sbin/$${p} \
	   ; $(LN_S) ../lib/portage/bin/$${p} $${p} || exit 1 \
	; done

.PHONY: all install
